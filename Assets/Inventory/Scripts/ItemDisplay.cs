using IGD347;
using UnityEngine;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour
{
    public InventoryScreen InventoryScreen;
    public ItemData NoItemData;
    public ItemData Data;
    public RawImage Icon;
    public int Index;

    public void SetupDisplay(ItemData itemData)
    {
        Data = itemData;
        UpdateDisplay();
    }

    public void UpdateDisplay()
    {
        if (Data != null)
            Icon.texture = Data.Icon;
        else
            Icon.texture = NoItemData.Icon;
    }

    public void OnClick()
    {
        if (Data != null)
            InventoryScreen.ShowItemInfo(Data, Index);
    }
}
