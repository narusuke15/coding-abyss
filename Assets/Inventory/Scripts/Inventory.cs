using System.Collections.Generic;
using IGD347;

public class Inventory
{
    public List<ItemInstance> ItemList;
    public int MaxInventorySize = 30;



    public void Init()
    {
        ItemList = new (30);
    }

    public void AddItem(ItemInstance item)
    {
        if (ItemList.Count == MaxInventorySize)
            return;
        ItemList.Add(item);
    }

    public void RemoveItem(int index)
    {
        ItemList.RemoveAt(index);
    }
}
