using System.Collections.Generic;
using IGD347;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScreen : MonoBehaviour
{
    public TMP_Text NameText, DescriptionText;
    public RawImage IconImage;
    public Button UseButton, DropButton;
    public ItemData NoItemData;
    public List<ItemDisplay> ItemDisplayList;

    int _selectedIndex;

    void Start()
    {
        ClearItemInfo();
        UpdateDisplay();
    }

    void UpdateDisplay()
    {
        var itemList = GameplayManager.Instance.PlayerInventory.ItemList;
        int inventorySize = GameplayManager.Instance.PlayerInventory.MaxInventorySize;
        Debug.Log("itemList count = " + itemList.Count);
        for (int i = 0; i < inventorySize; i++)
        {
            ItemDisplayList[i].Index = i;
            if (itemList.Count > i)
                ItemDisplayList[i].SetupDisplay(itemList[i].Data);
            else
                ItemDisplayList[i].SetupDisplay(null);
        }
    }

    public void ClearItemInfo()
    {
        // NameText.SetText("");
        // DescriptionText.SetText("");
        // IconImage.texture = NoItemTexture;
        // UseButton.interactable = false;
        // DropButton.interactable = false;
        // _selectedIndex = -1;
        ShowItemInfo(NoItemData, -1);
    }

    public void ShowItemInfo(ItemData itemData, int index)
    {
        NameText.SetText(itemData.ItemName);
        DescriptionText.SetText(itemData.Description);
        IconImage.texture = itemData.Icon;
        UseButton.interactable = itemData.Usable;
        DropButton.interactable = itemData.Droppable;
        _selectedIndex = index;
    }

    public void DropSelectedItem()
    {
        GameplayManager.Instance.PlayerInventory.RemoveItem(_selectedIndex);
        UpdateDisplay();
        ClearItemInfo();
    }

    public void UseSelectedItem()
    {
        var item = GameplayManager.Instance.PlayerInventory.ItemList[_selectedIndex];
        item.Use();
        if (item.Data.Consumable)
            GameplayManager.Instance.PlayerInventory.RemoveItem(_selectedIndex);
        UpdateDisplay();
        ClearItemInfo();
    }
}
