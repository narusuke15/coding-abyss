using UnityEngine;
using UnityEngine.Playables;

public class RaycastCutsceneTrigger : MonoBehaviour
{
    public PlayableDirector CutSceneDirector;

    public void TriggerCutScene()
    {
        GetComponent<Collider>().enabled = false;
        CutSceneDirector.Play();
    }
}
