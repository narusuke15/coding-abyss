using UnityEngine;
using TMPro;

public class VersionText : MonoBehaviour
{
    public TMP_Text VersionTextComponent;
    void OnEnable()
    {
        VersionTextComponent.text = $"V {Application.version}";
    }
}
