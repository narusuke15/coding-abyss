using UnityEngine;
using StarterAssets;
using IGD347;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Instance;

    [Header("Scene Reference")]
    public FirstPersonController PlayerController;
    public PlayerRaycaster PlayerRaycaster;

    [Header("Prefabs")]
    public GameObject ObjectViewerPrefabs;
    public GameObject InventoryScreenPrefabs;
    public GameObject PauseGamePopupPrefabs;

    GameObject _inventoryScreen;
    GameObject _pauseGamePopup;

    // player
    public Inventory PlayerInventory;
    public int Sanity
    {
        get
        {
            return _sanity;
        }
        set
        {
            _sanity = Mathf.Clamp(value, 0, 100);
        }
    }
    int _sanity = 50;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        PlayerInventory = new Inventory();
        PlayerInventory.Init();
    }

    void Update()
    {
        // open pause menu
        //FIXME: กด esc ตอนเปิด menu อื่นได้
        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePause();

        if (Input.GetKeyDown(KeyCode.Tab))
            ToggleInventory();
    }

    public void OpenNoteView(GameObject NoteCanvasPrefabs)
    {
        StopGame();
        Instantiate(NoteCanvasPrefabs); // สร้าง ui
    }

    public void CloseView(GameObject ViewObject)
    {
        ResumeGame();
        Destroy(ViewObject);
    }

    public void OpenObjectView(ItemData itemData)
    {
        StopGame();
        ObjectViewer objectViewer = Instantiate(ObjectViewerPrefabs).GetComponent<ObjectViewer>(); // สร้าง ui
        objectViewer.CreateObject(itemData);
    }

    public void ToggleInventory()
    {
        if (!_inventoryScreen)
        {
            StopGame();
            _inventoryScreen = Instantiate(InventoryScreenPrefabs);
        }
        else
        {
            Destroy(_inventoryScreen);
            _inventoryScreen = null;
            ResumeGame();
        }
    }

    public void TogglePause()
    {
        if (!_pauseGamePopup)
        {
            StopGame();
            _pauseGamePopup = Instantiate(PauseGamePopupPrefabs);
        }
        else
        {
            Destroy(_pauseGamePopup);
            _pauseGamePopup = null;
            ResumeGame();
        }
    }

    void StopGame()
    {
        // ปิดการใช้งานผู้เล่น
        PlayerController.enabled = false;
        PlayerRaycaster.enabled = false;
        Time.timeScale = 0; // หยุดเวลา
        Cursor.visible = true; // เปิด cursor
        Cursor.lockState = CursorLockMode.None; // ปลดล็อค cursor
    }

    void ResumeGame()
    {
        // เปิดการใช้งานผู้เล่น
        PlayerController.enabled = true;
        PlayerRaycaster.enabled = true;
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
