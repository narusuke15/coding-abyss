using UnityEngine;

public class BlinkingLight : MonoBehaviour
{
    public Material _material;
    public float IntensityChangeSpeed = 0.1f;
    public bool _isMoveUp = true;
    float currentIntensity = 0;
    public Color color;

    void Awake()
    {
        _material = GetComponent<MeshRenderer>().materials[1];
        color = _material.GetColor("_EmissionColor");
    }

    void Update()
    {
        if (_isMoveUp)
        {
            currentIntensity += IntensityChangeSpeed;
            if (currentIntensity > 100)
                _isMoveUp = false;
            else
                _material.SetColor("_EmissiveColor", color * currentIntensity);
        }
        else
        {
            currentIntensity -= IntensityChangeSpeed;
            if (currentIntensity <= 0)
                _isMoveUp = true;
            else
                _material.SetColor("_EmissiveColor", color * currentIntensity);
        }
    }
}
