using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupExitButton : MonoBehaviour
{
    public void ExitPopup(GameObject Popup)
    {
        GameObject.Destroy(Popup);
    }
}
