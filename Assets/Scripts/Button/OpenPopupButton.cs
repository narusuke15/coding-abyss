using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPopupButton : MonoBehaviour
{
    public void ShowPopup(GameObject Popup)
    {
        Instantiate(Popup);
    }
}
