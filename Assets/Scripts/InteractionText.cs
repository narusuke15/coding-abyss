using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InteractionText : MonoBehaviour
{
    public Image Crosshair;
    TMP_Text text;


    void Start()
    {
        text = GetComponent<TMP_Text>();
    }

    public void Show(string message)
    {
        if (!text.isActiveAndEnabled)
        {
            Crosshair.enabled = true;
            text.enabled = true;
        }
        text.text = message;
    }

    public void Hide()
    {
        if (text.isActiveAndEnabled)
        {
            Crosshair.enabled = false;
            text.enabled = false;
        }
    }
}
