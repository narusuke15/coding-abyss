using UnityEngine;

public class PlayerRaycaster : MonoBehaviour
{
    public Transform CameraTransform;
    public InteractionText InteractionText;
    public LayerMask RaycastLayerMask;
    public float InteractionDistance = 5f;

    [Header("Debug Data")]
    public string CurrentHitName;


    void Update()
    {
        if (Physics.Raycast(CameraTransform.position, CameraTransform.forward, out RaycastHit hit, InteractionDistance, RaycastLayerMask))
        {
            Collider other = hit.collider;
            CurrentHitName = other.name;
            if (other.CompareTag(Tag.RaycastTrigger))
                other.GetComponent<RaycastCutsceneTrigger>().TriggerCutScene();
            else if (other.CompareTag(Tag.Interactable))
            {
                var interactable = other.GetComponent<IInteractable>();
                InteractionText.Show($"[E] {interactable.GetInteractionText()}");
                if (Input.GetKeyDown(KeyCode.E))
                    interactable.Interact();
                Debug.DrawRay(CameraTransform.position, CameraTransform.forward * InteractionDistance, Color.green);
            }
            else
            {
                InteractionText.Hide();
                Debug.DrawRay(CameraTransform.position, CameraTransform.forward * InteractionDistance, Color.yellow);
            }
        }
        else
        {
            CurrentHitName = "";
            InteractionText.Hide();
            Debug.DrawRay(CameraTransform.position, CameraTransform.forward * InteractionDistance, Color.gray);
        }
    }
}
