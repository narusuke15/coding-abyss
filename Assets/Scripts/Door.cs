using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour, IInteractable
{
    public Animator DoorAnimator;
    public Collider DoorCollider;
    public bool IsLock;

    [SerializeField]
    bool IsOpen
    {
        get { return _isOpen; }
        set
        {
            _isOpen = value;
            DoorAnimator.SetBool("IsOpen", _isOpen);
        }
    }
    bool _isOpen;

    public string GetInteractionText()
    {
        if (IsLock)
            return "ประตูล็อค";
        if (_isOpen)
            return "ปิดประตู";
        else
            return "เปิดประตู";
    }

    public void Interact()
    {
        if(IsLock)
            return;
        IsOpen = !IsOpen;
        StartCoroutine(DisableColliderRoutine(0.5f));
    }

    IEnumerator DisableColliderRoutine(float delaySeconds)
    {
        Collider doorKnobCollider = GetComponent<Collider>();
        doorKnobCollider.enabled = false;
        DoorCollider.enabled = false;
        yield return new WaitForSeconds(delaySeconds);
        doorKnobCollider.enabled = true;
        DoorCollider.enabled = true;
    }
}
