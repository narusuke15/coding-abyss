using TMPro;
using UnityEngine;

public class SettingsPopup : MonoBehaviour
{
    public TMP_Dropdown GraphicsDropdown;

    void Start()
    {
        GraphicsDropdown.value = QualitySettings.GetQualityLevel();
    }

    public void ChangeGraphicSettings(int level)
    {
        QualitySettings.SetQualityLevel(level);
    }
}
