using System.Collections;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class FPSCounter : MonoBehaviour
{
    private TMP_Text _fpsText;
    void Start()
    {
        _fpsText = GetComponent<TMP_Text>();
        StartCoroutine(FramesPerSecond());
    }

    IEnumerator FramesPerSecond()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            float avgFPS = Mathf.RoundToInt(Time.frameCount / Time.time);
            _fpsText.SetText("FPS: {0}", avgFPS);
        }
    }
}
