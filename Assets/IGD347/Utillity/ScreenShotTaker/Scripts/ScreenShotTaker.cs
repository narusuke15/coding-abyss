using System;
using System.IO;
using TMPro;
using UnityEngine;

public class ScreenShotTaker : MonoBehaviour
{
    public RenderTexture renderTexture; // Reference to your Render Texture
    public TMP_InputField FileNameInputField;
    string _fileName = "ScreenShot.png";

    void Start()
    {
        FileNameInputField.onValueChanged.AddListener(SetFilename);

        if (!Directory.Exists(Path.Combine(Application.dataPath, "ScreenShots")))
        {
            Directory.CreateDirectory(Path.Combine(Application.dataPath, "ScreenShots"));
            Debug.Log("Create ScreenShot directory.");
        }
    }

    public void SaveToPNG()
    {
        // Make sure a Render Texture is assigned
        if (renderTexture == null)
        {
            Debug.LogError("Render Texture is not assigned.");
            return;
        }

        // Create a Texture2D to hold the Render Texture data
        Texture2D tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGBA32, false);

        // Read the Render Texture data into the Texture2D
        RenderTexture.active = renderTexture;
        tex.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        tex.Apply();

        // Reset the active Render Texture
        RenderTexture.active = null;

        // Encode the Texture2D as a PNG
        byte[] bytes = tex.EncodeToPNG();

        // Save the PNG to a file (change the path as needed)
        string filePath = Path.Combine(Application.dataPath, "ScreenShots", _fileName);
        File.WriteAllBytes(filePath, bytes);

        Debug.Log("Render Texture saved as PNG: " + filePath);
    }

    public void SetFilename(string fileName)
    {
        _fileName = fileName + ".png";
    }
}
