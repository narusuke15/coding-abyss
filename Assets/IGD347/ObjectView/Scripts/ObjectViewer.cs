using IGD347;
using TMPro;
using UnityEngine;

public class ObjectViewer : MonoBehaviour
{
    [Header("Scene Ref")]
    public Transform ModelParent;
    public TMP_Text TitleText, BodyText;

    public float RotationSpeed = 2.0f;

    private Transform _targetTransform;
    private bool _isRotating = false;
    private Vector3 _startRotation;

    public void CreateObject(ItemData itemData)
    {
        GameObject target = Instantiate(itemData.Model, ModelParent);
        TitleText.SetText(itemData.ItemName);
        BodyText.SetText(itemData.Description);
        _targetTransform = target.transform;
    }

    void Update()
    {
        // ทำงานเมื่อมี transform เท่านั้น
        if (_targetTransform == null)
            return;

        // เริ่มหมุนเมื่อคลิกซ้าย
        if (Input.GetMouseButtonDown(0))
        {
            _isRotating = true;
            _startRotation = ModelParent.rotation.eulerAngles;
            Cursor.visible = false;
        }

        // หยุดหมุนเมื่อปล่อยคลิกซ้าย
        if (Input.GetMouseButtonUp(0))
        {
            _isRotating = false;
            Cursor.visible = true;
        }

        if (_isRotating)
        {
            // รับค่าการเคลื่อนที่ mouse
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            _startRotation.y += mouseX * RotationSpeed;
            _startRotation.x -= mouseY * RotationSpeed; // Invert ค่าหมนุนแนวตั้ง

            // ใส่ค่าการหมุนให้วัตถุ (ต้องแปลงเป็น Quaternion ก่อน)
            ModelParent.rotation = Quaternion.Euler(_startRotation);
        }
    }

    public void Close()
    {
        GameplayManager.Instance.CloseView(gameObject);
    }
}
