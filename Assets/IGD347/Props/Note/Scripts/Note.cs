using UnityEngine;

public class Note : MonoBehaviour, IInteractable
{
    public GameObject NoteViewPrefab;
    public string GetInteractionText()
    {
        return "อ่าน";
    }

    public void Interact()
    {
        GameplayManager.Instance.OpenNoteView(NoteViewPrefab);
    }
}
