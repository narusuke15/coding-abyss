using UnityEngine;

public class NoteViewCanvas : MonoBehaviour
{
    public void Close()
    {
        GameplayManager.Instance.CloseView(gameObject);
    }
}
