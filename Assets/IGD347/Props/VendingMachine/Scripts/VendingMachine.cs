using UnityEngine;

namespace IGD347
{
    public class VendingMachine : MonoBehaviour, IInteractable
    {
        public Transform SpawnPoint;
        public GameObject CanPrefabs;
        public string GetInteractionText()
        {
            return "กดปุ่ม";
        }

        public void Interact()
        {
            Debug.Log("Spawn Can");
            float xPos = Random.Range(-0.3f, 0.3f);
            GameObject can = Instantiate(CanPrefabs, SpawnPoint);
            can.transform.localPosition = new Vector3(0, 0, xPos);
            can.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
    }
}