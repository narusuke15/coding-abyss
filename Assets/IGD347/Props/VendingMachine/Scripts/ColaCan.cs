using IGD347;
using UnityEngine;

public class ColaCan : MonoBehaviour, IInteractable
{
    public ItemInstance Item;
    
    public string GetInteractionText()
    {
        return "เก็บ";
    }

    public void Interact()
    {
        GameplayManager.Instance.PlayerInventory.AddItem(Item);
        Destroy(gameObject);
    }
}
