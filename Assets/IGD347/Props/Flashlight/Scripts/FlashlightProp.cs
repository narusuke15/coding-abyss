using UnityEngine;

public class FlashlightProp : MonoBehaviour, IInteractable
{
    public GameObject FlashlightEquipment;
    public ItemInstance Item;
    public string GetInteractionText()
    {
        return "เก็บไฟฉาย";
    }

    public void Interact()
    {
        GameplayManager.Instance.PlayerInventory.AddItem(Item);
        GameplayManager.Instance.OpenObjectView(Item.Data);
        FlashlightEquipment.SetActive(true);
        Destroy(gameObject);
    }
}
