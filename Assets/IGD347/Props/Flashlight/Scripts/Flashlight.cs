using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public bool IsOn = true;
    public Light Light;

    void Start()
    {
        Light.enabled = IsOn;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            IsOn = !IsOn;
            Light.enabled = IsOn;
        }
    }
}
