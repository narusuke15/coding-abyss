using UnityEngine;

namespace IGD347
{
    public class InfoPopupController : MonoBehaviour
    {
        Canvas _canvas;
        void Start()
        {
            _canvas = GetComponent<Canvas>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
                _canvas.enabled = !_canvas.enabled;
        }
    }
}
