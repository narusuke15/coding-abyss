using UnityEngine;

namespace IGD347
{
    [CreateAssetMenu(fileName = "Data", menuName = "IGD347/ItemData")]
    public class ItemData : ScriptableObject
    {
        public GameObject Model;
        public Texture2D Icon;
        public string ItemName;
        public string Description;
        public bool Consumable;
        public bool Usable;
        public bool Droppable;
    }
}